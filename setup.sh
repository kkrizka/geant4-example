# .bashrc

export PS1="(geant4) [\u@\h \W]\$ "

# ATLAS
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
if [ -d ${ATLAS_LOCAL_ROOT_BASE} ]; then
    source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh;
else
    echo "Error: cvmfs atlas repo is unavailable";
    exit 1
fi

# ROOT
alias root='root -l --web=off'

# Geant4 via LCG
lsetup "views LCG_105a x86_64-centos7-gcc12-opt"
