# Geant4 Quick Start

## Environment Setup

Use the Geant4 installation from CVMFS. You need to be running on an OS compatible with one of the builds. In this example, we'll use the CentOS7 build running inside a container created for the ATLAS experiment.

Start an Apptainer container, forwarding some of the common disks.

```shell
apptainer shell --cleanenv -B/disk/moose -B/cvmfs /cvmfs/unpacked.cern.ch/registry.hub.docker.com/atlasadc/atlas-grid-centos7:latest
```

Initialize the shell. This will set a few useful environmetal variables (ie: shell prompt via `PS1`) and load Geant4 via LCG. You need to do this everytime you start a new container.

```shell
source setup.sh
```

## Compiling B1 example

Use CMake to compile the B1 example.

```shell
cmake -S B1 -B B1_build
cmake --build B1_build -j5
```
